import React from 'react';
import { mount,unmount } from 'enzyme';
import App from 'components/App';
import Root from 'Root';

let wrapper;
const initialState = {
    worklogs: [
        {   
            id: 1,
            durationHours:'1',
            durationMinutes:'10',
            worktype:'Run',
            date: "12/05/2018"
        },
        {   id: 2,
            durationHours:'1',
            durationMinutes:'20',
            worktype: 'Swimming',
            date: "12/01/2011"
        }]
};

beforeEach(() => {
    wrapper = mount (
        <Root initialState={initialState}>
            <App />
        </Root>
    );
});

afterEach(() => {
    wrapper.unmount();
})

it('can add a worklog and show it', () => {
    wrapper.find('.duration-hours').simulate('change', {
        target: {
            name:'durationHours',
            value: '0'
        }
    });
    wrapper.find('.duration-minutes').simulate('change', {
        target: {
            name:'durationMinutes',
            value: '20'
        }
    });
    wrapper.find('Select').simulate('change', {
        target: {
            name:'worktype',
            value: 'Run'
        }
    });

    wrapper.find('DatePicker').simulate('change', '16/06/2018');

    wrapper.find('form').simulate('submit');
    wrapper.update();
    expect(wrapper.find('.worklog-line').length).toEqual(3);
})

it('it can delete a worklog from the list', () => {
    wrapper.find('.worklog-line-delete-button-1').simulate('click');
    wrapper.update();
    expect(wrapper.find('.worklog-line').length).toEqual(1);
})