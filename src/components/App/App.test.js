import React from "react";
import { shallow } from "enzyme";
import App from "components/App";
import WorkLogBox from 'components/WorkLogBox';
import WorkLogList from 'components/WorkLogList'

let wrapped;

beforeEach(() => {
    wrapped = shallow(<App />);
});

it('shows a comment box',() => {
    expect(wrapped.find(WorkLogBox).length).toEqual(1);
});

it('shows a comment list',() => {
    expect(wrapped.find(WorkLogList).length).toEqual(1);
})