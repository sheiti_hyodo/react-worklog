import React from 'react';
import WorkLogBox from 'components/WorkLogBox';
import WorkLogList from 'components/WorkLogList';
import { Paper} from '@material-ui/core';
import  "./App.css";

export default () => {
    return (
        <div className="App">
            <Paper className="work-log" elevation={5}>
                <WorkLogBox />
                <WorkLogList/>
            </Paper>
        </div>
    );
};