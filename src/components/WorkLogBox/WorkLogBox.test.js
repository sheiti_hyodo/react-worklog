import React from "react";
import { mount,unmount } from "enzyme";
import WorkLogBox from 'components/WorkLogBox';
import Root from 'Root';
import moment from 'moment';

let wrapper;

beforeEach(() => {
    wrapper = mount(
    <Root>
        <WorkLogBox />
    </Root>
    );
});

afterEach(()=>{
    wrapper.unmount();
})

it('shows a input for Hours spend',() => {
    expect(wrapper.find('.duration-hours').length).toEqual(1);
});

it('shows a input for Minutes spend',() => {
    expect(wrapper.find('.duration-minutes').length).toEqual(1);
});

it('shows a select for Work Type',() => {
    expect(wrapper.find('Select').length).toEqual(1);
})

it('shows a DatePicker',() => {
    expect(wrapper.find('DatePicker').length).toEqual(1);
})

it('show a button to add worklog',() => {
    expect(wrapper.find('button').length).toEqual(1);
})

describe('the time input ', () => {
    beforeEach(() => {
        wrapper.find('.duration-hours').simulate('change', {
            target: {
                name: 'durationHours',
                value: '12'
            }
        });

        wrapper.find('.duration-minutes').simulate('change', {
            target: {
                name: 'durationMinutes',
                value: '35'
            }
        });

        wrapper.update();
    });

    it(' allow users to input duration of work log', () => {
        expect(wrapper.find('.duration-hours').prop('value')).toEqual('12');
        expect(wrapper.find('.duration-minutes').prop('value')).toEqual('35');
    });

    it('clears the text on submit', () => {
        wrapper.find('form').simulate('submit');
        wrapper.update();
        expect(wrapper.find('.duration-hours').prop('value')).toEqual('');
        expect(wrapper.find('.duration-minutes').prop('value')).toEqual('');
    });
})

describe('the datepicker', () => {
    const date = moment().format('DD/MM/YYYY');
    beforeEach(() => {
        wrapper.find('input .work-day').simulate('change', {
            target: {
                name: 'date',
                value: '12/12/2018'
            }
        });
        wrapper.update();
    });

    it(' allow users select a date', () => {
        expect(wrapper.find('input .work-day').prop('value')).toEqual('12/12/2018');
    });

    it('clears the select on submit', () => {
        wrapper.find('form').simulate('submit');
        wrapper.update();
        expect(wrapper.find('input .work-day').prop('value')).toEqual(date);
    });
})

//Problem still open Reop of Material UI
// describe('the workType select ', () => {
//     beforeEach(() => {
//         wrapper.find('Select').simulate('change', {
//             target: {
//                 name: 'worktype',
//                 value: 'Run'
//             }
//         });
//         wrapper.update();
//     });

//     it(' allow users select a work type', () => {
//         console.log("teste",wrapper.find('Select').at(0).html());
//         expect(wrapper.find('#worktype').prop('value')).toEqual('Run');
//     });

//     it('clears the select on submit', () => {
//         wrapper.find('form').simulate('submit');
//         wrapper.update();
//         expect(wrapper.find('Select').prop('value')).toEqual('');
//     });
// })