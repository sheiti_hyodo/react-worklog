import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from 'actions';
import { Input, Button } from '@material-ui/core';
import { Select, MenuItem } from '@material-ui/core/';
import DatePicker from 'react-datepicker';
import './WorkLogBox.css';
import 'react-datepicker/dist/react-datepicker.css';

class WorkLogBox extends Component {

  workTypeOptions() {
    return this.props.options.map((option, index) => {
      return <MenuItem key={option} value={option}>{option}</MenuItem>
    })
  };

  handleChange = ({target}) => {
    const newWorklog =  { ...this.props.worklog, [target.name]: target.value};
    this.props.saveWorkLogForm(newWorklog);
  };

  handleChangeDate = date => {
    const newWorklog =  { ...this.props.worklog, date};
    this.props.saveWorkLogForm(newWorklog);
  };

  handleSubmit = event => {
    event.preventDefault();
    const worklog = { ...this.props.worklog, 
      date: this.props.worklog.date.format("DD/MM/YYYY")   
    }
    this.props.saveWorkLog(worklog);
    this.props.resetWorkLogForm();
  };

  render() {
    const {durationHours, durationMinutes,worktype,date} = this.props.worklog;
    return (
      <div className="work-log-box" >
        <h1>Workout Log</h1>
        <div>
          <h3 >Insert a Item</h3>
        </div>
        <form onSubmit={this.handleSubmit} className="work-log-form">
          <div className="row">
            <div className="content">
              <Input 
                inputProps={{className:"duration-hours"}}
                name="durationHours"
                placeholder="0" 
                required={true} 
                onChange={this.handleChange} 
                value={durationHours}
                type="number"
                />
                <div className="work-log-input-label">
                  <h3>h</h3>
                </div>
            </div>
            <div className="content">
              <Input
                inputProps={{className:"duration-minutes"}}
                name="durationMinutes"
                placeholder="0"
                required={true} 
                onChange={this.handleChange} 
                value={durationMinutes} 
                type="number"
              />
              <div className="work-log-input-label">
                <h3>min</h3>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="content">
                <Select
                  name="worktype"
                  className="work-type"
                  inputProps={{
                      id:"work-type",
                  }}
                  value={worktype}
                  onChange={this.handleChange}
                >
                  {this.workTypeOptions()}
                </Select>
            </div>
            <div className="content">
              <DatePicker className="work-day"
                selected={date}
                onChange={this.handleChangeDate}
                dateFormat="DD/MM/YYYY"
                required={true}
              />
            </div>
          </div>
          <div className="row">  
            <div className="content">
              <button type="submit">Add</button>
            </div>
          </div>
        </form>
        <hr/>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    worklog: state.worklogForm.worklog,
    options: state.worklogForm.options
  }
}

export default connect ( mapStateToProps, actions) (WorkLogBox);
