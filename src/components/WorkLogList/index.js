import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import { selectWorkOutTotal } from 'reducers';
import './WorkLogList.css';

class WorkLogList extends Component {
  handleDelete = event => {
    event.preventDefault();
    this.props.deleteWorkLog(event.target.value);
  }
  renderWorkLogs() {
    return this.props.worklogs.map((log, index) => {
      return (
        <tr className="worklog-line" key={"worklog-line-" + index}>
          <td key={"worklog-line-duration-" + index}>
            {log.durationHours} h {log.durationMinutes}
          </td>
          <td key={"worklog-line-worktype-" + index}>{log.worktype}</td>
          <td key={"worklog-line-date-" + index}>{log.date}</td>
          <td key={"worklog-line-delete-" + index}>
            <button key={"worklog-line-delete-button-" + index}
              className={"worklog-line-delete-button-" + index}
              value={log.id}
              onClick={this.handleDelete}
            >-</button>
          </td>
        </tr>
      )
    })
  };

  render() {
    return (
      <div>
        <div className="work-log-list ">
          <table className="table-striped">
            <thead> 
              <tr key="workLogListHead">
                <th>Duration</th>
                <th>Workout</th>
                <th>Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody key="workLogListBody">
              {this.renderWorkLogs()}
            </tbody>
          </table>
        </div>
        <div>
          <h3 className="worklog-sum">{this.props.workoutTotal} of exercises</h3>  
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  debugger
  return { 
    worklogs: state.worklogs,
    workoutTotal: selectWorkOutTotal(state)
  };
}

export default connect (mapStateToProps,actions) (WorkLogList);
