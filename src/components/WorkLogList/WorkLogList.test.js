import React from "react";
import { mount, unmount } from "enzyme";
import WorkLogList from 'components/WorkLogList';
import Root from 'Root';

let wrapper;
const initialState = {
    worklogs: [
        {   
            id: 1,
            durationHours: 1,
            durationMinutes: 10,
            worktype:'Run',
            date: "12/05/2018"
        },
        {   id: 2,
            durationHours: 1,
            durationMinutes: 15,
            worktype: 'Swimming',
            date: "12/01/2011"
        }]
};

beforeEach(() => {
    wrapper = mount(
        <Root initialState={initialState}>
            <WorkLogList/>
        </Root>
    );
});

afterEach(()=>{
    wrapper.unmount();
})

it('creates one worklogline per log', () => {
     expect(wrapper.find('.worklog-line').length).toEqual(2);
 })
 
it('shows the text for each log', () => {
    expect(wrapper.render().text()).toContain('1 h 10');
    expect(wrapper.render().text()).toContain('1 h 15');
});

it('shows Sum of workout ', () => {
    expect(wrapper.find('.worklog-sum').length).toEqual(1);
    expect(wrapper.find('.worklog-sum').text()).toContain('2 h 25 min');
});