export const SAVE_WORKLOG = 'save_worklog';
export const DELETE_WORKLOG = 'delete_worklog';
export const SAVE_WORKLOG_FORM = 'save_worklog_form';
export const RESET_WORKLOG_FORM = 'reset_worklog_form';