import { saveWorkLog, deleteWorkLog } from 'actions';
import { saveWorkLogForm, resetWorkLogForm } from 'actions';
import { SAVE_WORKLOG, DELETE_WORKLOG } from 'actions/types';
import { SAVE_WORKLOG_FORM, RESET_WORKLOG_FORM } from 'actions/types';

describe('saveWorkLog', () => {
    it('has the correct type', () => {
        const action = saveWorkLog();

        expect(action.type).toEqual(SAVE_WORKLOG);
    });

    it('has the correct payload', () => {
        const action = saveWorkLog('Log 1');

        expect(action.payload).toEqual('Log 1');
    });

})

describe('deleteWorkLog', () => {
    it('has the correct type', () => {
        const action = deleteWorkLog();

        expect(action.type).toEqual(DELETE_WORKLOG);
    });

    it('has the correct payload',() => {
        const action = deleteWorkLog('Log 1');

        expect(action.payload).toEqual('Log 1');
    })
})

describe('saveWorkLogForm', () => {
    it('has the correct type', () => {
        const action = saveWorkLogForm();

        expect(action.type).toEqual(SAVE_WORKLOG_FORM);
    });

    it('has the correct payload', () => {
        const action = saveWorkLogForm('Log 1');

        expect(action.payload).toEqual('Log 1');
    });

})

describe('resetWorkLogForm', () => {
    it('has the correct type', () => {
        const action = resetWorkLogForm();

        expect(action.type).toEqual(RESET_WORKLOG_FORM);
    });

    it('has the correct payload',() => {
        const action = resetWorkLogForm('Log 1');

        expect(action.payload).toEqual('');
    })
})