import { SAVE_WORKLOG,DELETE_WORKLOG, SAVE_WORKLOG_FORM, RESET_WORKLOG_FORM } from 'actions/types';

export function saveWorkLog(worklog) {
    return {
        type: SAVE_WORKLOG,
        payload: worklog 
    };
}

export const deleteWorkLog = id => {
    //Obs.: the right thing here should send a action of delete to the backend
    // on return call a action to update worklogs list
    
    return {
        type: DELETE_WORKLOG,
        payload: id 
    }
}

export function saveWorkLogForm(worklog) {
    return {
        type: SAVE_WORKLOG_FORM,
        payload: worklog 
    };
}

export function resetWorkLogForm() {
    return {
        type: RESET_WORKLOG_FORM,
        payload: ""
    };
}