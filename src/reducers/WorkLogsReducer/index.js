import { SAVE_WORKLOG, DELETE_WORKLOG } from 'actions/types';

export default function (state =[] ,action) {
    switch (action.type) {
        case SAVE_WORKLOG:
        //Adding Id to worklogs
        //Should not has it here
            const id = Math.floor((Math.random() * 100) + 1);
            action.payload['id'] = id;
            return [...state,action.payload]
        case DELETE_WORKLOG:
            const newState = state.filter((obj) => {
                return obj.id !== Number(action.payload)
            })
            return newState;
        default:
            return state;
    }
}

function  sumWorkLogOnMinutes (total, value) {
    return total + (Number(value.durationHours) * 60 )+ Number(value.durationMinutes);
}
  
function sumWorkLogTotalFormat (total) {
    return Math.floor(total/60) + " h " + (total%60)+ " min ";
}

export const selectWorkOutTotal = (state) => sumWorkLogTotalFormat(state.worklogs.reduce(sumWorkLogOnMinutes,0));

