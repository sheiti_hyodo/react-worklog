import WorkLogsReducer, {selectWorkOutTotal} from 'reducers/WorkLogsReducer';
import { SAVE_WORKLOG, DELETE_WORKLOG } from 'actions/types';

it('handles actions of type SAVE_WORKLOG', () => {
    const action = {
        type: SAVE_WORKLOG,
        payload: {
            durationHours: '1',
            durationMinutes: '12',
            worktype: 'Run',
            date: '12/08/2018',  
        }
    }

    const newState = WorkLogsReducer([], action);
    expect(newState[0].id).toBeGreaterThanOrEqual(1);
    expect(newState[0].durationHours).toEqual('1');
    expect(newState[0].durationMinutes).toEqual('12');
    expect(newState[0].worktype).toEqual('Run');
    expect(newState[0].date).toEqual('12/08/2018');
});

it('handles actions of type DELETE_WORKLOG', () => {
    const action = {
        type: DELETE_WORKLOG,
        payload:  20
    }

    const newState = WorkLogsReducer([{durationHours:'1', id:20}], action);
    expect(newState).toEqual([]);
});


it('handles action with unknown type', () => {
    const newState = WorkLogsReducer([],{ type: 'FatLazy'});

    expect(newState).toEqual([]);
});

it('compute the workout total time', () => {
    const state = {
        worklogs: [{durationHours:1, durationMinutes: 40 },{durationHours:0, durationMinutes: 45 }]
    }
    const workOutTotal = selectWorkOutTotal(state);
    expect(workOutTotal).toContain("2 h 25 min");
});