import { SAVE_WORKLOG_FORM, RESET_WORKLOG_FORM  } from 'actions/types';
import moment from 'moment';

const options = ['Run','Swimming','Bike']
const date = moment();
const initalState = {
    worklog:{
        durationHours: '',
        durationMinutes: '',
        worktype: options[0],
        date,  
    },
    options
}
export default function (state =initalState ,action) {
    switch (action.type) {
        case SAVE_WORKLOG_FORM:
            const worklog = action.payload
            return {...state,worklog}
        case RESET_WORKLOG_FORM:
            return initalState
        default:
            return state;
    }
}
