import WorkLogsFormReducer from 'reducers/WorkLogFormReducer';
import { SAVE_WORKLOG_FORM, RESET_WORKLOG_FORM } from 'actions/types';
import moment from 'moment';

const options = ['Run','Swimming','Bike']
const date = moment();
let initalState = {
    worklog:{
        durationHours: '',
        durationMinutes: '',
        worktype: options[0],
        date,  
    },
    options
}

it('handles actions of type SAVE_WORKLOG_FORM', () => {
    const action = {
        type: SAVE_WORKLOG_FORM,
        payload: {
            durationHours: '1',
            durationMinutes: '12',
            worktype: options[2],
            date: '12/08/2018',  
        }
    }

    const newState = WorkLogsFormReducer(initalState, action);
    expect(newState.worklog.durationHours).toEqual('1');
    expect(newState.worklog.durationMinutes).toEqual('12');
    expect(newState.worklog.worktype).toEqual(options[2]);
    expect(newState.worklog.date).toEqual('12/08/2018');
});

it('handles actions of type RESET_WORKLOG_FORM', () => {
    const action = {
        type: RESET_WORKLOG_FORM,
        payload:  "bla"
    }

    const newState = WorkLogsFormReducer(initalState, action);
    expect(newState.worklog.durationHours).toEqual('');
    expect(newState.worklog.durationMinutes).toEqual('');
    expect(newState.worklog.worktype).toEqual(options[0]);
    expect(moment(newState.worklog.date).format('DD/MM/YYYY')).toContain(date.format('DD/MM/YYYY'));
});


it('handles action with unknown type', () => {
    const newState = WorkLogsFormReducer([],{ type: 'FatLazy'});

    expect(newState).toEqual([]);
});