import { combineReducers } from 'redux';
import WorkLogsReducer, * as fromWorkLogs from 'reducers/WorkLogsReducer';
import WorkLogFormReducer from'reducers/WorkLogFormReducer';

export default combineReducers ({
    worklogs: WorkLogsReducer,
    worklogForm:  WorkLogFormReducer,
});

export const selectWorkOutTotal = (state) => fromWorkLogs.selectWorkOutTotal(state);